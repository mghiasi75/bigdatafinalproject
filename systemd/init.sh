sudo cp *.service /etc/systemd/system/

sudo systemctl daemon-reload

sudo systemctl enable bigdata-crawler.service
sudo systemctl restart bigdata-crawler.service

sudo systemctl enable bigdata-preprocess.service
sudo systemctl restart bigdata-preprocess.service

sudo systemctl enable bigdata-persistence.service
sudo systemctl restart bigdata-persistence.service

sudo systemctl enable bigdata-channelhistory.service
sudo systemctl restart bigdata-channelhistory.service

sudo systemctl enable bigdata-statistics.service
sudo systemctl restart bigdata-statistics.service

sudo systemctl enable bigdata-analytics.service
sudo systemctl restart bigdata-analytics.service

sudo systemctl enable bigdata-webapp.service
sudo systemctl restart bigdata-webapp.service
