from kafka import KafkaProducer, KafkaConsumer
import json
import uuid
import re
from hazm import word_tokenize, POSTagger
import os
from bs4 import BeautifulSoup
import time
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer


COLLECT_TEXT_FOR_IDF = False

def extractLinks(html):
    soup = BeautifulSoup(html)
    anchors = soup.findAll('a')
    links = []
    for a in anchors:
        if a['href'].startswith('/hashtag/'):
            continue
        links.append(a['href'])
    return links

TAG_RE = re.compile(r'<[^>]+>')

tagger = POSTagger(model=os.path.dirname(os.path.abspath(__file__)) + "/postagger.model")

namads = open(os.path.dirname(os.path.abspath(__file__)) + '/namads.txt', 'r').readlines()
namads = [line[:-1] for line in namads]

importantKeywords = open(os.path.dirname(os.path.abspath(__file__)) + '/important_keywords.txt', 'r').readlines()
importantKeywords = [line[:-1] for line in importantKeywords]

stopwords = open(os.path.dirname(os.path.abspath(__file__)) + '/stopwords.txt', 'r').readlines()
stopwords = [line[:-1] for line in stopwords]

# TF-IDF functions taken from https://kavita-ganesan.com/extracting-keywords-from-text-tfidf/
def sort_coo(coo_matrix):
    tuples = zip(coo_matrix.col, coo_matrix.data)
    return sorted(tuples, key=lambda x: (x[1], x[0]), reverse=True)

def extract_topn_from_vector(feature_names, sorted_items, topn=10):
    """get the feature names and tf-idf score of top n items"""
    #use only topn items from vector
    sorted_items = sorted_items[:topn]
    score_vals = []
    feature_vals = []
    # word index and corresponding tf-idf score
    for idx, score in sorted_items:
        #keep track of feature name and its corresponding score
        score_vals.append(round(score, 3))
        feature_vals.append(feature_names[idx])
    #create a tuples of feature,score
    #results = zip(feature_vals,score_vals)
    results= {}
    for idx in range(len(feature_vals)):
        results[feature_vals[idx]]=score_vals[idx]
    return results


idfdata =  open(os.path.dirname(os.path.abspath(__file__)) + '/idf_data.txt', 'r').read().replace("\n", " ")
cv = CountVectorizer()
word_count_vector = cv.fit_transform([idfdata])
tfidf_transformer=TfidfTransformer(smooth_idf=True,use_idf=True)
tfidf_transformer.fit(word_count_vector)
feature_names=cv.get_feature_names()

consumer = KafkaConsumer('preprocess',
                         bootstrap_servers=['localhost:9092'])
producer = KafkaProducer(bootstrap_servers='localhost:9092')

while True:
    msg_pack = consumer.poll(timeout_ms=500)

    for tp, messages in msg_pack.items():
        for message in messages:
            tweet = json.loads(message.value.decode('utf-8'))
            tweet['timestamp'] = int(time.time() * 1000)
            tweet['uuid'] = str(uuid.uuid4())
            tweet.pop('id', None)

            tweet['text'] = TAG_RE.sub('', tweet['content'])

            tokenized_text = word_tokenize(tweet['text'])
            tweet['hashtags'] = []
            tweet['namads'] = []
            tweet['keywords'] = []
            for word in tokenized_text:
                word_norm = word
                if word[0] == '#':
                    tweet['hashtags'].append(word)
                    word_norm = word[1:]
                
                if word_norm in namads:
                    tweet['namads'].append(word_norm)

                if word_norm in importantKeywords:
                    tweet['keywords'].append(word_norm)

            tweet['images'] = []
            if len(tweet['image']) > 0:
                tweet['images'].append(tweet['image'])
            tweet.pop('image', None)

            tweet['links'] = extractLinks(tweet['content'])

            text_nohashtag = tweet['text'].replace('#', ' ')
            taggedWords = tagger.tag(word_tokenize(text_nohashtag))
            
            # Collect all tweets for IDF calculation
            if COLLECT_TEXT_FOR_IDF:
                with open(os.path.dirname(os.path.abspath(__file__)) + '/idf_data.txt', 'a') as idf_file:
                    for word, tag in taggedWords:
                        if word in stopwords:
                            continue
                        if len(word) <= 1:
                            continue
                        if tag == "N" or tag == "ADV" or tag == "AJ":
                            idf_file.write(word + "\n")

            text_imp = ""
            for word, tag in taggedWords:
                if word in stopwords:
                    continue
                if len(word) <= 1:
                    continue
                if tag == "N" or tag == "ADV" or tag == "AJ":
                    text_imp += word + " "
            tf_idf_vector = tfidf_transformer.transform(cv.transform([text_imp]))
            sorted_items = sort_coo(tf_idf_vector.tocoo())
            keywords = extract_topn_from_vector(feature_names,sorted_items,5)
            #print(keywords.keys())
            tweet['keywords'] = list(set(list(keywords.keys()) + tweet['keywords']))

            #print(tweet['namads'])
            #print(tweet['links'])
            #print(tweet['text'])
            #print(tweet['images'])
            #print(tweet['keywords'])
            #print(tweet['timestamp'])
            #print("")
            print(tweet['uuid'])
            producer.send('persistence', json.dumps(tweet).encode('utf-8'))
            