from flask import Flask, request
from waitress import serve
from jinja2 import Environment, FileSystemLoader, select_autoescape
import os
import redis
import json
import hashlib
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
from datetime import datetime, timedelta
from elasticsearch import Elasticsearch
from datetime import datetime

HTTP_PORT = 5000
REDIS_STORE_PREFIX = "tweets_stats"
ES_INDEX_NAME="tweets"

r = redis.Redis(password="infinityandbeyond")

auth_provider = PlainTextAuthProvider(username='cassandra', password='infinityandbeyond')
cassandraCluster = Cluster(auth_provider=auth_provider)
cassandraSession = cassandraCluster.connect()
cassandraSession.set_keyspace('saham')

es = Elasticsearch(['http://localhost:9200'])

jinja_env = Environment(
    loader=FileSystemLoader(os.path.dirname(os.path.abspath(__file__)) + '/templates/'),
    autoescape=select_autoescape(['html', 'xml'])
)

app = Flask(__name__)

def get_recent_hashtags():
    hashtags = r.lrange(REDIS_STORE_PREFIX + "___LATEST_HASHTAGS", 0, -1)
    return [x.decode('utf-8') for x in hashtags]

def get_recent_tweets():
    raw_tweets = r.lrange(REDIS_STORE_PREFIX + "___LATEST_POSTS", 0, -1)
    tweets = []
    for raw_tweet in raw_tweets:
        tweets.append(json.loads(raw_tweet.decode('utf-8')))
    return tweets

def get_recent_users(recent_tweets):
    users = []
    for tweet in recent_tweets:
        if tweet['userName'] in users:
            continue
        users.append(tweet['userName'])
    return users

def get_recent_namads(recent_tweets):
    namads = []
    for tweet in recent_tweets:
        for namad in tweet['namads']:
            if namad in namads:
                continue
            namads.append(namad)
    return namads

def get_tweets_count(hours):
    count = 0
    dt = datetime.now()
    for i in range(hours):
        key_dayhour = str(dt.day + dt.month * 100 + dt.year * 10000) + "_" + str(dt.hour)
        count_data = r.hget(REDIS_STORE_PREFIX + "_" + key_dayhour, "TWEETS")
        if not count_data == None:
            count += int(count_data.decode('utf-8'))
        dt = dt + timedelta(hours=-1)
    return count

def get_unique_hashtags_count():
    dt = datetime.now()
    key_dayhour = str(dt.day + dt.month * 100 + dt.year * 10000) + "_" + str(dt.hour)
    count_data = r.scard(REDIS_STORE_PREFIX + "___LATEST_HASHTAGS_" + key_dayhour)
    if count_data == None:
        return 0
    return count_data

def get_recent_unique_hashtags():
    dt = datetime.now()
    key_dayhour = str(dt.day + dt.month * 100 + dt.year * 10000) + "_" + str(dt.hour)
    hashtags = r.smembers(REDIS_STORE_PREFIX + "___LATEST_HASHTAGS_" + key_dayhour)
    return [x.decode('utf-8') for x in hashtags]

def get_group_tweet_count(group, hash, hours):
    count = 0
    dt = datetime.now()
    for i in range(hours):
        key_dayhour = str(dt.day + dt.month * 100 + dt.year * 10000) + "_" + str(dt.hour)
        count_data = r.hget(REDIS_STORE_PREFIX + "_" + key_dayhour, "TWEETS_" + group + "_" + hash)
        if not count_data == None:
            count += int(count_data.decode('utf-8'))
        dt = dt + timedelta(hours=-1)
    return count

def get_user_tweet_count(user, hours):
    hash = str(hashlib.md5(user.encode('utf-8')).hexdigest())
    return get_group_tweet_count("USER", hash, hours)

def get_namad_tweet_count(namad, hours):
    hash = str(hashlib.md5(namad.encode('utf-8')).hexdigest())
    return get_group_tweet_count("NAMAD", hash, hours)

@app.route('/')
def main():
    template = jinja_env.get_template('main.html')
    return template.render()#tests=fetch_tests(), token=request.args.get('token'))

@app.route('/stats')
def stats():
    recent_tweets = get_recent_tweets()
    recent_users = get_recent_users(recent_tweets)
    recent_namads = get_recent_namads(recent_tweets)

    for tweet in recent_tweets:
        tweet['date'] = datetime.fromtimestamp(tweet['timestamp'] / 1000)
    
    user = request.args.get('user')
    if user == None:
        user = recent_users[0]
    namad = request.args.get('namad')
    if namad == None:
        namad = recent_namads[0]
    
    stats = {
        'tweetsToday': get_tweets_count(24),
        'uniqueHashtagsThisHour': get_unique_hashtags_count(),
        'userTweets6Hours': get_user_tweet_count(user, 6),
        'namadTweets1Hour': get_namad_tweet_count(namad, 1),
        'namadTweets6Hours': get_namad_tweet_count(namad, 6),
        'namadTweets24Hours': get_namad_tweet_count(namad, 24),
    }

    template = jinja_env.get_template('stats.html')
    return template.render(
        stats=stats,
        selected_user=user,
        selected_namad=namad,
        namads=recent_namads,
        users=recent_users, 
        tweets=recent_tweets,
        hashtags=get_recent_hashtags())

@app.route('/history')
def history():
    recent_tweets = get_recent_tweets()
    recent_users = get_recent_users(recent_tweets)
    recent_namads = get_recent_namads(recent_tweets)
    
    dates = []
    dt = datetime.now()
    currentDate = str(dt.year) + "/" + str(dt.month) + "/" + str(dt.day) + "  " + str(dt.hour) + ":" + str(dt.minute)
    for i in range(60):
        item = dict()
        item['value'] = str(dt.day + dt.month * 100 + dt.year * 10000)
        item['text'] = str(dt.year) + "/" + str(dt.month) + "/" + str(dt.day)
        dates.append(item)
        dt = dt + timedelta(days=-1)

    selected_date = request.args.get('date')
    if selected_date == None:
        selected_date = dates[0]['value']
    selected_date_obj = {
        'value': selected_date, 
        'text': str(int(selected_date) // 10000) + "/" + str((int(selected_date) // 100) % 100) + "/" + str(int(selected_date) % 100)
    }

    selected_hour = request.args.get('hour')
    if selected_hour == None:
        selected_hour = ''
    selected_user = request.args.get('user')
    if selected_user == None:
        selected_user = ''
    selected_hashtag = request.args.get('hashtag')
    if selected_hashtag == None:
        selected_hashtag = ''
    selected_namad = request.args.get('namad')
    if selected_namad == None:
        selected_namad = ''

    tableName = "AllTweets"
    extraQuery = ""
    extraQueryData = []
    if selected_user != '':
        tableName = 'TweetsForUser'
        extraQuery = " AND user = ? "
        extraQueryData = [selected_user]
        selected_hashtag = ''
        selected_namad = ''
    elif selected_hashtag != '':
        tableName = 'TweetsForHashtag'
        extraQuery = " AND hashtag = ? "
        extraQueryData = [selected_hashtag]
        selected_namad = ''
    elif selected_namad != '':
        tableName = 'TweetsForNamad'
        extraQuery = " AND namad = ? "
        extraQueryData = [selected_namad]
      
    stmt = cassandraSession.prepare("SELECT tweetId FROM " + tableName + " WHERE date = ? " + (" AND hour = ? " if selected_hour != '' else "") + extraQuery)
    query = cassandraSession.execute(stmt, [int(selected_date)] + ([int(selected_hour)] if selected_hour != '' else []) + extraQueryData)
    
    tweetIds = [] 
    for row in query:
        tweetIds = tweetIds + row[0]
    
    tweets_count = len(tweetIds)
    count_filter_text = ""
    if tweets_count > 1000:
        tweetIds = tweetIds[-1000:]
        count_filter_text = "latest 1000 "
    tweetIds.reverse()
    searchTitle = "Showing " + count_filter_text + "tweets sent in " + selected_date_obj["text"] + (("  " + selected_hour + ":xx") if selected_hour != '' else "")
    if selected_user != '':
        searchTitle += " from user " + selected_user
    elif selected_hashtag != '':
        searchTitle += " with hashtag " + selected_hashtag
    elif selected_namad != '':
        searchTitle += " with Namad " + selected_namad

    tweets = []
    for tweetId in tweetIds:
        try:
            result = es.search(index="tweets", body=
            {
                "query": {
                    "match": {
                        "uuid": tweetId
                    }
                }
            })
            tweet = result['hits']['hits'][0]['_source']
            tweet['date'] = datetime.fromtimestamp(tweet['timestamp'] / 1000)
            tweets.append(tweet)
        except:
            pass

    template = jinja_env.get_template('history.html')
    return template.render(
        hashtags=get_recent_unique_hashtags(),
        namads=recent_namads,
        users=recent_users,
        hours=[str(x) for x in range(0,24)],
        dates=dates,
        results=tweets,
        searchTitle=searchTitle,
        currentDate=currentDate,
        selectedDate=selected_date_obj,
        selectedHour=selected_hour,
        selectedHashtag=selected_hashtag,
        selectedUser=selected_user,
        selectedNamad=selected_namad,
        resultsCount=tweets_count)

if __name__ == '__main__':
    serve(app, host="0.0.0.0", port=HTTP_PORT)
