from kafka import KafkaProducer, KafkaConsumer
import json
from clickhouse_driver import Client

clickhouse = Client('localhost', 
    user='mahdi',
    password='infinityandbeyond')

clickhouse.execute("CREATE DATABASE IF NOT EXISTS saham")
clickhouse.execute("USE saham")
clickhouse.execute("""
    CREATE TABLE IF NOT EXISTS tweets (
        uuid String,
        timestamp DateTime,
        userName String,
        userDisplayName String,
        hashtag String,
        namad String,
        keyword String
    ) 
    ENGINE = MergeTree()
    PARTITION BY toYYYYMMDD(timestamp)
    ORDER BY timestamp
""")

consumer = KafkaConsumer('analytics',
                         bootstrap_servers=['localhost:9092'])

while True:
    msg_pack = consumer.poll(timeout_ms=500)
    for tp, messages in msg_pack.items():
        for message in messages:          
            tweet = json.loads(message.value.decode('utf-8'))

            rows_to_insert = []
            for i in range(max(len(tweet['hashtags']), len(tweet['keywords']), len(tweet['namads']))):
                hashtag = ''
                if i < len(tweet['hashtags']):
                    hashtag = tweet['hashtags'][i]
                keyword = ''
                if i < len(tweet['keywords']):
                    keyword = tweet['keywords'][i]
                namad = ''
                if i < len(tweet['namads']):
                    namad = tweet['namads'][i]

                rows_to_insert.append({
                    "uuid": tweet["uuid"],
                    "timestamp": int(tweet["timestamp"] / 1000.0),
                    "userName": tweet["userName"],
                    "userDisplayName": tweet["userDisplayName"],
                    "hashtag": hashtag,
                    "keyword": keyword,
                    "namad": namad,
                })
                
            clickhouse.execute("""
                INSERT INTO tweets (uuid, timestamp, userName, userDisplayName, hashtag, namad, keyword) VALUES
            """, rows_to_insert)

            print(tweet['uuid'])
