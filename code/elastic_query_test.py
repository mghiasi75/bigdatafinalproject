from elasticsearch import Elasticsearch

es = Elasticsearch(['http://localhost:9200'])

result = es.search(index="tweets", body=
{
    "query": {
        "query_string": {
            "query": 'text: "سلام" and namads:"فولاد"',
        }
    }
}, size=100)
tweets = result['hits']['hits']
print(tweets)


count = es.count(index="tweets", body=
{
    "query": {
        "query_string": {
            "query": 'text: "سلام"',
        }
    }
})
print(count['count'])

print("")