from kafka import KafkaProducer, KafkaConsumer
from datetime import datetime
import json
import redis
import hashlib

REDIS_STORE_PREFIX = "tweets_stats"
REDIS_KEY_LIFETIME = 3600 * 24 * 7

r = redis.Redis(password="infinityandbeyond")

consumer = KafkaConsumer('statistics',
                         bootstrap_servers=['localhost:9092'])
producer = KafkaProducer(bootstrap_servers='localhost:9092')


while True:
    msg_pack = consumer.poll(timeout_ms=500)
    for tp, messages in msg_pack.items():
        for message in messages:
            producer.send('analytics', message.value)
            tweet = json.loads(message.value.decode('utf-8'))

            dt = datetime.fromtimestamp(tweet['timestamp'] / 1000.0)
            date = dt.day + dt.month * 100 + dt.year * 10000
            hour = dt.hour

            key_dayhour = str(date) + "_" + str(hour)
            userNameHash = str(hashlib.md5(tweet['userName'].encode('utf-8')).hexdigest())

            r.hincrby(REDIS_STORE_PREFIX + "_" + key_dayhour, "TWEETS", 1)
            r.hincrby(REDIS_STORE_PREFIX + "_" + key_dayhour, "TWEETS_USER_" + userNameHash, 1)
            r.expire(REDIS_STORE_PREFIX + "_" + key_dayhour, REDIS_KEY_LIFETIME)

            for namad in tweet['namads']:
                namadHash = str(hashlib.md5(namad.encode('utf-8')).hexdigest())
                r.hincrby(REDIS_STORE_PREFIX + "_" + key_dayhour, "TWEETS_NAMAD_" + namadHash, 1)
                # expire has set before

                r.hincrby(REDIS_STORE_PREFIX + "_" + str(date) + "_Q" + str(hour / 6), "TWEETS_NAMAD_" + namadHash, 1)
                r.expire(REDIS_STORE_PREFIX + "_" + str(date) + "_Q" + str(hour / 6), REDIS_KEY_LIFETIME)

                r.hincrby(REDIS_STORE_PREFIX + "_" + str(date), "TWEETS_NAMAD_" + namadHash, 1)
                r.expire(REDIS_STORE_PREFIX + "_" + str(date), REDIS_KEY_LIFETIME)

            for hashtag in tweet['hashtags']:
                r.sadd(REDIS_STORE_PREFIX + "___LATEST_HASHTAGS_" + key_dayhour, hashtag)
                r.expire(REDIS_STORE_PREFIX + "___LATEST_HASHTAGS_" + key_dayhour, REDIS_KEY_LIFETIME)
                # count of set can be retrieved by r.scard(key)

                hashtagHash = str(hashlib.md5(hashtag.encode('utf-8')).hexdigest())
                r.hincrby(REDIS_STORE_PREFIX + "_" + key_dayhour, "TWEETS_HASHTAG_" + hashtagHash, 1)
                # expire has been set before

                r.lpush(REDIS_STORE_PREFIX + "___LATEST_HASHTAGS", hashtag)
                r.ltrim(REDIS_STORE_PREFIX + "___LATEST_HASHTAGS", 0, 999)

            r.lpush(REDIS_STORE_PREFIX + "___LATEST_POSTS", json.dumps(tweet).encode('utf-8'))
            r.ltrim(REDIS_STORE_PREFIX + "___LATEST_POSTS", 0, 99)

            print(tweet['uuid'])
