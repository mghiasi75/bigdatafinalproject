import asyncio
from pyppeteer import launch
from kafka import KafkaProducer
import json
from time import sleep

async def processTweet(page, tweetsContainer, id):
    userDisplayNameElem = await page.querySelector("#" + id + " .lastNameUser")
    userDisplayName = await page.evaluate("function(x) { return x.innerText; }", userDisplayNameElem)

    userNameElem = await page.querySelector("#" + id + " .UserName")
    userName = await page.evaluate("function(x) { return x.innerText; }", userNameElem)

    tweetContentElem = await page.querySelector("#" + id + " .twitsContents")
    tweetContent = await page.evaluate("function(x) { return x.innerHTML; }", tweetContentElem)

    imageContainerElem = await page.querySelector("#" + id + " .imgComment")
    image = ''
    if (imageContainerElem != None):
        image = await page.evaluate("function(x) { return x.style.backgroundImage; }", imageContainerElem)
        image = image[5:]
        image = image[:-2]

    return { 
        'id': id,
        'userDisplayName': userDisplayName,
        'userName': userName,
        'content': tweetContent,
        'image': image,
    }

async def startListen(browser, producer):
    print("Opening sahamyab...")
    page = await browser.newPage()
    await page.goto('https://www.sahamyab.com/stocktwits')
    
    tweetsContainer = await page.querySelector("#twits-contents")

    watchdogCounter = 0
    while True:
        tweetItemIds = await page.evaluate("function(x) { return Array.from(x.querySelectorAll('.twitsItem')).map(x => x.id); }", tweetsContainer)

        for id in tweetItemIds:
            tweet = await processTweet(page, tweetsContainer, id)
            producer.send('preprocess', json.dumps(tweet).encode('utf-8'))
            print("processed tweet " + tweet['id'])

        for id in tweetItemIds:
            itemToDel = await page.querySelector("#" + id)
            await page.evaluate("function(x) { x.parentNode.removeChild(x); }", itemToDel)
        
        sleep(0.5)
        if len(tweetItemIds) > 0:
            watchdogCounter = 0
        else:
            watchdogCounter += 1
        
        if watchdogCounter > 200:
            print("watchdog expired. Will reopen the page.")
            await page.close()
            return

async def main():
    producer = KafkaProducer(bootstrap_servers='localhost:9092')
    browser = await launch()

    while True:
        try:
            await startListen(browser, producer)
        except (KeyboardInterrupt, SystemExit):
            raise
        except Exception as e:
            await browser.close()
            print(e)
            #print("Exception in crawler. Will reopen the page.")

            raise # process will get restarted automatically by systemd
        

asyncio.get_event_loop().run_until_complete(main())