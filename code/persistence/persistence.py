from elasticsearch import Elasticsearch
from kafka import KafkaProducer, KafkaConsumer
import json

INDEX_NAME="tweets"

consumer = KafkaConsumer('persistence',
                         bootstrap_servers=['localhost:9092'])
producer = KafkaProducer(bootstrap_servers='localhost:9092')

es = Elasticsearch(['http://localhost:9200'])

if not es.ping():
    print("Couldn't connect to elasticsearch.")
    exit()

# es.indices.delete(index=INDEX_NAME)
if not es.indices.exists(index=INDEX_NAME):
    request_body = {
        "settings" : {
            "number_of_shards": 4,
            "number_of_replicas": 1,
            "analysis": {
                "char_filter": {
                    "zero_width_spaces": {
                        "type":       "mapping",
                        "mappings": [ "\\u200C=>\\u0020"] 
                    }
                },
                "filter": {
                    "persian_stop": {
                    "type":       "stop",
                    "stopwords":  "_persian_" 
                    }
                },
                "analyzer": {
                    "rebuilt_persian": {
                        "tokenizer":     "standard",
                        "char_filter": [ "zero_width_spaces" ],
                        "filter": [
                            "lowercase",
                            "decimal_digit",
                            "arabic_normalization",
                            "persian_normalization",
                            "persian_stop"
                        ]
                    }
                }
            }
        },

        'mappings': { 
            "dynamic": "strict",
            'properties': {
                'uuid': {'type': 'text'},
                'timestamp': {'type': 'date'},
                'userName': {'type': 'keyword'},
                'userDisplayName': {'type': 'text'},
                'content': {'type': 'text'},
                'text': {'type': 'text'},
                'hashtags': {'type': 'keyword'},
                'namads': {'type': 'keyword'},
                'keywords': {'type': 'keyword'},
                'images': {'type': 'text'},
                'links': {'type': 'text'},
            }
        }
    }
    es.indices.create(index=INDEX_NAME, body=request_body)


while True:
    msg_pack = consumer.poll(timeout_ms=500)
    for tp, messages in msg_pack.items():
        for message in messages:
            producer.send('channelhistory', message.value)
            tweet = json.loads(message.value.decode('utf-8'))
            es.index(index=INDEX_NAME, body=tweet)
            print(tweet['uuid'])
        
            