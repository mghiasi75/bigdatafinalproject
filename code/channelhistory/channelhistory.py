from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
from kafka import KafkaProducer, KafkaConsumer
import json
import hashlib 
from datetime import datetime


auth_provider = PlainTextAuthProvider(username='cassandra', password='infinityandbeyond')
cluster = Cluster(auth_provider=auth_provider)
session = cluster.connect()

consumer = KafkaConsumer('channelhistory',
                         bootstrap_servers=['localhost:9092'])
producer = KafkaProducer(bootstrap_servers='localhost:9092')

session.execute("""
    CREATE KEYSPACE IF NOT EXISTS saham 
    WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 2 };
""")
session.set_keyspace('saham')

session.execute("""
    CREATE TABLE IF NOT EXISTS AllTweets (
        date int,
        hour int,
        tweetId list<text>,
        PRIMARY KEY (date, hour)
    )
""") 
session.execute("""
    CREATE TABLE IF NOT EXISTS TweetsForUser (
        date int,
        hour int,
        user text,
        tweetId list<text>,
        PRIMARY KEY (user, date, hour)
    )
""")
session.execute("""
    CREATE TABLE IF NOT EXISTS TweetsForHashtag (
        date int,
        hour int,
        hashtag text,
        tweetId list<text>,
        PRIMARY KEY (hashtag, date, hour)
    )
""")
session.execute("""
    CREATE TABLE IF NOT EXISTS TweetsForNamad (
        date int,
        hour int,
        namad text,
        tweetId list<text>,
        PRIMARY KEY (namad, date, hour)
    )
""")

while True:
    try:
        msg_pack = consumer.poll(timeout_ms=500)
        for tp, messages in msg_pack.items():
            for message in messages:
                tweet = json.loads(message.value.decode('utf-8'))

                dt = datetime.fromtimestamp(tweet['timestamp'] / 1000.0)
                date = dt.day + dt.month * 100 + dt.year * 10000
                hour = dt.hour

                # Update AllTweets
                res = session.execute("""
                    UPDATE AllTweets SET tweetId = tweetId + [%s] 
                    WHERE date = %s AND hour = %s
                """, [tweet['uuid'], date, hour])

                # Update TweetsForUser                
                session.execute("""
                    UPDATE TweetsForUser SET tweetId = tweetId + [%s] 
                    WHERE date = %s AND hour = %s AND user = %s
                """, [tweet['uuid'], date, hour, tweet['userName']])

                # Update TweetsForHashtag for each hashtag
                for hashtag in tweet['hashtags']:
                    session.execute("""
                        UPDATE TweetsForHashtag SET tweetId = tweetId + [%s] 
                        WHERE date = %s AND hour = %s AND hashtag = %s
                    """, [tweet['uuid'], date, hour, hashtag])

                # Update TweetsForNamad for each namad
                for namad in tweet['namads']:
                    session.execute("""
                        UPDATE TweetsForNamad SET tweetId = tweetId + [%s] 
                        WHERE date = %s AND hour = %s AND namad = %s
                    """, [tweet['uuid'], date, hour, namad])

                print(tweet['uuid'])
                producer.send('statistics', message.value)
    except (KeyboardInterrupt, SystemExit):
        raise
    except:
        print("exception occurred. will continue.")